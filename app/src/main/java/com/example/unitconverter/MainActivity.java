package com.example.unitconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button i2c, c2i;
    private TextView message;
    private EditText inchInput, cmInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        i2c = findViewById(R.id.i2c);
        c2i = findViewById(R.id.c2i);
        inchInput = findViewById(R.id.inchInput);
        cmInput = findViewById(R.id.cmInput);
        message = findViewById(R.id.message);

        i2c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double convertedInch = Double.parseDouble(inchInput.getText().toString());
                    if (convertedInch < 0)
                        throw new Exception("Error: negative number cannot be entered");
                    cmInput.setText(Double.toString(convertedInch * 2.54));
                    message.setText("");
                } catch (NumberFormatException e) {
                    if (e.getMessage() == "empty String") {
                        message.setText("Error: Enter inch value");
                    } else {
                        message.setText("Error: inch - Only number can be entered");
                    }
                } catch (Exception e) {
                    message.setText(e.getMessage());
                }
            }
        });

        c2i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double convertedCm = Double.parseDouble(cmInput.getText().toString());
                    if (convertedCm < 0)
                        throw new Exception("Error: negative number cannot be entered");
                    inchInput.setText(Double.toString(convertedCm / 2.54));
                    message.setText("");
                } catch (NumberFormatException e) {
                    if (e.getMessage() == "empty String") {
                        message.setText("Error: Enter cm value");
                    } else {
                        message.setText("Error: cm - Only number can be entered");
                    }
                } catch (Exception e) {
                    message.setText(e.getMessage());
                }
            }
        });
    }
}
